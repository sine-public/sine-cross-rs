#! /bin/bash
set -e
trap 'previous_command=$this_command; this_command=$BASH_COMMAND' DEBUG
trap 'echo FAILED COMMAND: $previous_command' EXIT

#-------------------------------------------------------------------------------------------
# This script will download packages for, configure, build and install a GCC cross-compiler.
# Customize the variables (INSTALL_PATH, TARGET, etc.) to your liking before running.
# If you get an error and need to resume the script from some point in the middle,
# just delete/comment the preceding lines before running it again.
#
# See: http://preshing.com/20141119/how-to-build-a-gcc-cross-compiler
#-------------------------------------------------------------------------------------------

INSTALL_PATH=/usr/local
TARGET=$1
LINUX_ARCH=$2
USE_NEWLIB=0
# --enable-gnu-unique-object is for specifying ABI as GNU/Linux instead of SYSV
CONFIGURATION_OPTIONS="--disable-multilib --enable-gnu-unique-object --enable-lto --disable-debug --enable-plugins --disable-decimal-float --enable-threads"
CONFIGURATION_OPTIONS_GCC=""
CFLAGS="-g0 -O3" # Disable debug_info for compiler/system libraries
PARALLEL_MAKE=-j$(nproc)
BINUTILS_VERSION=binutils-2.42
GCC_VERSION=gcc-13.3.0
LINUX_KERNEL_VERSION=linux-4.19.105
GLIBC_VERSION=glibc-2.28
MPFR_VERSION=mpfr-4.1.0
GMP_VERSION=gmp-6.2.1
MPC_VERSION=mpc-1.2.1
ISL_VERSION=isl-0.24
CLOOG_VERSION=cloog-0.18.1
LIBSTDCPP_PATH=lib/libstdc++.so.6.0.32
export PATH=$INSTALL_PATH/bin:$PATH

# Make ARM builds ARM-HF
if [ $LINUX_ARCH == "arm" ]; then
    CONFIGURATION_OPTIONS="$CONFIGURATION_OPTIONS --with-float=hard --with-abi=aapcs-linux --with-fpu=fp-armv8 --with-mode=arm"
    CONFIGURATION_OPTIONS_GCC="$CONFIGURATION_OPTIONS_GCC --with-cpu=cortex-a53"
fi

if [ $LINUX_ARCH == "arm64" ]; then
    LIBSTDCPP_PATH=lib64/libstdc++.so.6.0.32
fi

# Download packages
export http_proxy=$HTTP_PROXY https_proxy=$HTTP_PROXY ftp_proxy=$HTTP_PROXY
wget -nc https://ftp.gnu.org/gnu/binutils/$BINUTILS_VERSION.tar.gz
wget -nc https://ftp.gnu.org/gnu/gcc/$GCC_VERSION/$GCC_VERSION.tar.gz
if [ $USE_NEWLIB -ne 0 ]; then
    wget -nc -O newlib-master.zip https://github.com/bminor/newlib/archive/master.zip || true
    unzip -qo newlib-master.zip
else
    wget -nc https://www.kernel.org/pub/linux/kernel/v4.x/$LINUX_KERNEL_VERSION.tar.xz
    wget -nc https://ftp.gnu.org/gnu/glibc/$GLIBC_VERSION.tar.xz
fi
wget -nc https://ftp.gnu.org/gnu/mpfr/$MPFR_VERSION.tar.xz
wget -nc https://ftp.gnu.org/gnu/gmp/$GMP_VERSION.tar.xz
wget -nc https://ftp.gnu.org/gnu/mpc/$MPC_VERSION.tar.gz
wget -nc ftp://gcc.gnu.org/pub/gcc/infrastructure/$ISL_VERSION.tar.bz2
wget -nc ftp://gcc.gnu.org/pub/gcc/infrastructure/$CLOOG_VERSION.tar.gz

# Extract everything
for f in *.tar*; do tar xfk $f; done

# Make symbolic links
cd $GCC_VERSION
ln -sf `ls -1d ../mpfr-*/` mpfr
ln -sf `ls -1d ../gmp-*/` gmp
ln -sf `ls -1d ../mpc-*/` mpc
ln -sf `ls -1d ../isl-*/` isl
ln -sf `ls -1d ../cloog-*/` cloog
cd ..

# Step 1. Binutils
mkdir -p build-binutils
cd build-binutils
../$BINUTILS_VERSION/configure --prefix=$INSTALL_PATH --target=$TARGET $CONFIGURATION_OPTIONS CFLAGS="$CFLAGS" CXXFLAGS="$CFLAGS"
make $PARALLEL_MAKE
make install
cd ..

# Step 2. Linux Kernel Headers
if [ $USE_NEWLIB -eq 0 ]; then
    cd $LINUX_KERNEL_VERSION
    make ARCH=$LINUX_ARCH INSTALL_HDR_PATH=$INSTALL_PATH/$TARGET headers_install
    cd ..
fi

# Step 3. C/C++ Compilers
mkdir -p build-gcc
cd build-gcc
if [ $USE_NEWLIB -ne 0 ]; then
    NEWLIB_OPTION=--with-newlib
fi
../$GCC_VERSION/configure --prefix=$INSTALL_PATH --target=$TARGET --enable-languages=c $CONFIGURATION_OPTIONS $CONFIGURATION_OPTIONS_GCC $NEWLIB_OPTION --enable-languages=c CFLAGS="$CFLAGS" CXXFLAGS="$CFLAGS"
make $PARALLEL_MAKE all-gcc
make install-gcc
cd ..

if [ $USE_NEWLIB -ne 0 ]; then
    # Steps 4-6: Newlib
    mkdir -p build-newlib
    cd build-newlib
    ../newlib-master/configure --prefix=$INSTALL_PATH --target=$TARGET $CONFIGURATION_OPTIONS CFLAGS="$CFLAGS" CXXFLAGS="$CFLAGS"
    make $PARALLEL_MAKE
    make install
    cd ..
else
    # Step 4. Standard C Library Headers and Startup Files
    mkdir -p build-glibc
    cd build-glibc
    ../$GLIBC_VERSION/configure --prefix=$INSTALL_PATH/$TARGET --build=$MACHTYPE --host=$TARGET --target=$TARGET --with-headers=$INSTALL_PATH/$TARGET/include $CONFIGURATION_OPTIONS libc_cv_forced_unwind=yes CFLAGS="$CFLAGS" CXXFLAGS="$CFLAGS" --disable-werror
    make install-bootstrap-headers=yes install-headers
    make $PARALLEL_MAKE csu/subdir_lib
    install csu/crt1.o csu/crti.o csu/crtn.o $INSTALL_PATH/$TARGET/lib
    $TARGET-gcc -nostdlib -nostartfiles -shared -x c /dev/null -o $INSTALL_PATH/$TARGET/lib/libc.so
    touch $INSTALL_PATH/$TARGET/include/gnu/stubs.h
    cd ..

    # Step 5. Compiler Support Library
    cd build-gcc
    make $PARALLEL_MAKE all-target-libgcc
    make install-target-libgcc
    cd ..

    # Step 6. Standard C Library & the rest of Glibc
    cd build-glibc
    make $PARALLEL_MAKE
    make install
    cd ..
fi

# Step 7. Standard C++ Library & the rest of GCC
cd build-gcc
../$GCC_VERSION/configure --prefix=$INSTALL_PATH --target=$TARGET --enable-languages=c,c++ $CONFIGURATION_OPTIONS $CONFIGURATION_OPTIONS_GCC $NEWLIB_OPTION --enable-languages=c,c++ -includedir=$INSTALL_PATH/$TARGET/include CFLAGS="$CFLAGS" CXXFLAGS="$CFLAGS"
make $PARALLEL_MAKE all
make install
cd ..

$INSTALL_PATH/$TARGET/bin/strip --strip-debug $INSTALL_PATH/$TARGET/$LIBSTDCPP_PATH

rm -rf $GCC_VERSION.tar.gz \
    $BINUTILS_VERSION.tar.gz \
    $LINUX_KERNEL_VERSION.tar.xz \
    $GLIBC_VERSION.tar.xz \
    $MPFR_VERSION.tar.xz \
    $GMP_VERSION.tar.xz \
    $MPC_VERSION.tar.gz \
    $ISL_VERSION.tar.bz2 \
    $CLOOG_VERSION.tar.gz \
    newlib-master.zip \
    build-binutils \
    $LINUX_KERNEL_VERSION \
    build-gcc \
    build-glibc \
    $GCC_VERSION \
    $BINUTILS_VERSION \
    $GLIBC_VERSION \
    $MPFR_VERSION \
    $GMP_VERSION \
    $MPC_VERSION \
    $ISL_VERSION \
    $CLOOG_VERSION

trap - EXIT
echo 'Cross-GCC compiled successfully!'
