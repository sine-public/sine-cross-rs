#!/bin/bash

docker build ./../docker -f ./../docker/Dockerfile.x86_64-pc-windows-gnu -t sineengineering/cross-x86_64-pc-windows-gnu:latest
docker build ./../docker -f ./../docker/Dockerfile.x86_64-unknown-linux-gnu -t sineengineering/cross-x86_64-unknown-linux-gnu:latest
docker build ./../docker -f ./../docker/Dockerfile.aarch64-unknown-linux-gnu -t sineengineering/cross-aarch64-unknown-linux-gnu:latest
docker build ./../docker -f ./../docker/Dockerfile.arm-unknown-linux-gnueabihf -t sineengineering/cross-arm-unknown-linux-gnueabihf:latest

# Tag images with date and Git short hash in addition to revision
TAG=$(git log -1 --date=format:'%Y%m%d' --format='%ad')-$(git rev-parse --short HEAD)

docker build ./../docker -f ./../docker/Dockerfile.x86_64-pc-windows-gnu -t sineengineering/cross-x86_64-pc-windows-gnu:$TAG
docker build ./../docker -f ./../docker/Dockerfile.x86_64-unknown-linux-gnu -t sineengineering/cross-x86_64-unknown-linux-gnu:$TAG
docker build ./../docker -f ./../docker/Dockerfile.aarch64-unknown-linux-gnu -t sineengineering/cross-aarch64-unknown-linux-gnu:$TAG
docker build ./../docker -f ./../docker/Dockerfile.arm-unknown-linux-gnueabihf -t sineengineering/cross-arm-unknown-linux-gnueabihf:$TAG
