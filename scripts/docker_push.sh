#!/usr/bin/env bash

# Tag images with date and Git short hash in addition to revision
TAG=$(git log -1 --date=format:'%Y%m%d' --format='%ad')-$(git rev-parse --short HEAD)

CONTAINERS=("x86_64-pc-windows-gnu" "x86_64-unknown-linux-gnu" "aarch64-unknown-linux-gnu" "arm-unknown-linux-gnueabihf")

for CONTAINER in ${CONTAINERS[@]}; do
	docker push sineengineering/cross-$CONTAINER:$TAG
	docker push sineengineering/cross-$CONTAINER:latest
done
